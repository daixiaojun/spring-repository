package cn.yunhe.dao;

import cn.yunhe.beans.Item;

/**
 * @author Juni
 * @date 2021/1/19
 */
public interface ItemDao {

    /**
     * 添加商品
     * @param item
     * @return
     */
    public int addItem(Item item);

    /**
     * 删除指定商品
     * @param itemId
     * @return
     */
    public int deleteItem(int itemId);
}
