package cn.yunhe.dao.impl;

import cn.yunhe.beans.User;
import cn.yunhe.dao.IUserDao;
import org.springframework.stereotype.Component;

/**
 * @author Juni
 * @date 2021/1/18
 */
@Component("userDao2")
public class UserDaoImpl implements IUserDao {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int insertUser(User user) {
        System.out.println("添加用户信息");
        return 0;
    }

    public int deleteUser(int uid) {
        System.out.println("删除指定用户信息");
        return 0;
    }

    public void show(){
        System.out.println("show--->"+user);
    }
}
