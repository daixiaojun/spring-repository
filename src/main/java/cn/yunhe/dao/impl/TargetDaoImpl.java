package cn.yunhe.dao.impl;

import cn.yunhe.dao.ITargetDao;
import org.springframework.stereotype.Repository;

/**
 * @author Juni
 * @date 2021/1/19
 */
@Repository
public class TargetDaoImpl implements ITargetDao {

    @Override
    public void gucci() {
        //System.out.println("去哪能买到，还不用出门，买到的真不真");
        System.out.println("购买奢侈品：gucci");
        //System.out.println("验证真假，消毒等等");
    }

    @Override
    public void pulada() {
        //System.out.println("去哪能买到，还不用出门，买到的真不真");
        System.out.println("购买奢侈品：pulada");
        //System.out.println("验证真假，消毒等等");
    }
}
