package cn.yunhe.dao.impl;

import cn.yunhe.beans.Item;
import cn.yunhe.dao.ItemDao;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author Juni
 * @date 2021/1/19
 */
@Repository
public class ItemDaoImpl implements ItemDao {

    @Override
    public int addItem(Item item) {
        System.out.println("添加商品成功");
        return 0;
    }

    @Override
    public int deleteItem(int itemId) {
        System.out.println("删除商品成功");
        //int a = 1/0;
        return 0;
    }
}
