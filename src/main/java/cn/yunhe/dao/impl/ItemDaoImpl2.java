package cn.yunhe.dao.impl;

import cn.yunhe.beans.Item;
import cn.yunhe.dao.ItemDao;
import org.springframework.stereotype.Repository;

/**
 * @author Juni
 * @date 2021/1/19
 */
@Repository(value = "cglibItemDao")
public class ItemDaoImpl2 {

    public int addItem(Item item) {
        System.out.println("添加商品成功");
        return 0;
    }

    public int deleteItem(int itemId) {
        System.out.println("删除商品成功");
        return 0;
    }
}
