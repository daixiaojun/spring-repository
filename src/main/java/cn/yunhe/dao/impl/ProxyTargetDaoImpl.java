package cn.yunhe.dao.impl;

import cn.yunhe.dao.ITargetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author Juni
 * @date 2021/1/19
 * 代理对象（代购）
 * 静态代理：和目标对象实现同一个接口，这样代理对象就知道目标对象的需求了
 */
@Component
public class ProxyTargetDaoImpl implements ITargetDao {

    @Autowired
    ITargetDao targetDao;

    @Override
    public void gucci() {
        System.out.println("去哪能买到，还不用出门，买到的真不真");
        //指定买奢侈品的功能
        targetDao.gucci();
        System.out.println("验证真假，消毒等等");
    }

    @Override
    public void pulada() {
        System.out.println("去哪能买到，还不用出门，买到的真不真");
        targetDao.pulada();
        System.out.println("验证真假，消毒等等");
    }
}
