package cn.yunhe.dao;

import cn.yunhe.beans.User;
import org.springframework.stereotype.Component;

/**
 * @author Juni
 * @date 2021/1/18
 */

public interface IUserDao {

    /**
     * 添加用户信息
     * @param user
     * @return
     */
    public int insertUser(User user);

    /**
     * 删除指定用户id
     * @param uid
     * @return
     */
    public int deleteUser(int uid);
}
