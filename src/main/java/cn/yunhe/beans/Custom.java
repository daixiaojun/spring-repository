package cn.yunhe.beans;

import java.util.List;
import java.util.Map;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class Custom {

    private int age;

    private String name;

    private List<String> list;

    private Map<String,Object> map;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "Custom{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", list=" + list +
                ", map=" + map +
                '}';
    }
}
