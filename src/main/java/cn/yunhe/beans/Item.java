package cn.yunhe.beans;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class Item {

    private int itemId;

    private String itemName;

    private double itemPrice;

    public Item(){
        System.out.println("Item 构造器");
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                '}';
    }

    public void init(){
        System.out.println("item 初始化");
    }

    public void destroy(){
        System.out.println("item 销毁");
    }
}
