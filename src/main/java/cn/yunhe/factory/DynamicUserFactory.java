package cn.yunhe.factory;

import cn.yunhe.beans.User;

/**
 * @author Juni
 * @date 2021/1/18
 */
public class DynamicUserFactory {

    public User newInstance(){
        return new User(16,"小黑");
    }
}
