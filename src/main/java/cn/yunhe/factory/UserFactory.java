package cn.yunhe.factory;

import cn.yunhe.beans.User;

/**
 * @author Juni
 * @date 2021/1/18
 */
public class UserFactory {

    /**
     * 返回user对象
     * @return
     */
    public static User newInstance(){
        return new User(15,"小溪");
    }

    public static User newInstance2(){
        return new User();
    }
}
