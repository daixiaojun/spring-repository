package cn.yunhe.test;

import cn.yunhe.beans.Item;
import cn.yunhe.dao.ItemDao;
import cn.yunhe.dao.impl.ItemDaoImpl;
import cn.yunhe.dao.impl.ItemDaoImpl2;
import org.junit.Test;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Juni
 * @date 2021/1/20
 * 动态代理测试
 */
public class DynamicProxyTest {

    @Test
    public void jdkProxyTest(){
        //目标对象
        ItemDao itemDao = new ItemDaoImpl();
        //代理对象-jdk代理(JDK中提供了一个Proxy类用于处理代理功能)
        ItemDao proxyItemDao = (ItemDao) Proxy.newProxyInstance(
                //类加载器，需要指定目标对象的类加载器，加载其class文件，为目标对象去生成代理对象
                itemDao.getClass().getClassLoader(),
                //指定目标对象所有的接口文件，代理对象就可以知道目标对象中的所有功能，从而选择要进行代理的功能
                itemDao.getClass().getInterfaces(),
                //通过提供的InvocationHandler接口去指定要进行代理的功能及对应的代理方案
                new InvocationHandler() {
                    //proxy 指的是代理对象
                    //method 目标对象中的方法
                    //args 要代理的方法对象的参数
                    @Override
                    //代理的过程
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        //代理之前的准备工作
                        System.out.println("去哪能买到，还不用出门，买到的真不真");
                        //指定要代理的功能 invoke（其实是内部使用反射进行获取方法）
                        Object object = method.invoke(itemDao,args);
                        //功能执行之后要做的事情
                        System.out.println("验证真假，消毒等等");
                        //返回的Object对象就是代理对象
                        return object;
                    }
                }
        );
        //执行需要进行代理的功能
        proxyItemDao.deleteItem(1);
        proxyItemDao.addItem(new Item());
    }

    /***
     * cglib代理是Spring中提供的代理方案
     * cglib代理不需要有接口的
     */
    @Test
    public void cglibProxyTest(){
        //目标对象
        ItemDaoImpl2 itemDaoImpl2 = new ItemDaoImpl2();
        //代理对象
        Enhancer enhancer = new Enhancer();
        //设置要代理的目标对象
        enhancer.setSuperclass(itemDaoImpl2.getClass());
        //设置具体的代理过程
        enhancer.setCallback(new MethodInterceptor() {
            //用来处理代理过程
            @Override
            public Object intercept(Object proxy, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                //代理之前的准备工作
                System.out.println("去哪能买到，还不用出门，买到的真不真");
                //指定要代理的功能 invoke（其实是内部使用反射进行获取方法）
                //Object object = method.invoke(itemDaoImpl2,objects);
                Object object = methodProxy.invokeSuper(proxy,objects);
                //功能执行之后要做的事情
                System.out.println("验证真假，消毒等等");
                return object;
            }
        });
        //创建代理对象
        ItemDaoImpl2 proxy = (ItemDaoImpl2)enhancer.create();
        //执行代理方法
        proxy.deleteItem(1);
    }

















}
