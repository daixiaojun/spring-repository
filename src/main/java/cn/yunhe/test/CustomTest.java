package cn.yunhe.test;

import cn.yunhe.beans.Custom;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class CustomTest {

    @Test
    public void test(){
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext2.xml");
        Custom custom = ac.getBean("custom", Custom.class);
        System.out.println(custom);
        //关闭读取对象
        ac.close();
    }
}
