package cn.yunhe.test;

import cn.yunhe.beans.Item;
import cn.yunhe.dao.impl.ItemDaoImpl;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Juni
 * @date 2021/1/20
 */
public class AdviceTest {

    @Test
    public void adviceTest(){
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext8.xml");
        ItemDaoImpl itemDao = ac.getBean("itemDao", ItemDaoImpl.class);
        itemDao.deleteItem(1);
        ac.close();
    }
}
