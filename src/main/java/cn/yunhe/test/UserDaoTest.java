package cn.yunhe.test;

import cn.yunhe.beans.User;
import cn.yunhe.dao.IUserDao;
import cn.yunhe.dao.impl.UserDaoImpl;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Juni
 * @date 2021/1/18
 */
public class UserDaoTest {

    @Test
    public void insertUserTest(){
        UserDaoImpl userDao = new UserDaoImpl();
        userDao.insertUser(new User());
        userDao.deleteUser(1);
    }

    @Test
    public void springTest(){
        //读取配置文件
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        //参数一：配置文件中的唯一标识 参数二：要接收的数据类型
        IUserDao userDao = ac.getBean("userDao",UserDaoImpl.class);
        userDao.insertUser(new User());
        userDao.deleteUser(1);
    }

    @Test
    public void springTest2(){
        //读取配置文件
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        //参数一：配置文件中的唯一标识 参数二：要接收的数据类型
        User user = ac.getBean("user",User.class);
        System.out.println(user);

        User user2 = ac.getBean("user2",User.class);
        System.out.println(user2);

        User user3 = ac.getBean("user3",User.class);
        System.out.println(user3);

        User user4 = ac.getBean("user4",User.class);
        System.out.println(user4);

        UserDaoImpl userDao = ac.getBean("userDao2",UserDaoImpl.class);
        userDao.show();
    }

    @Test
    public void springTest3(){
        //读取配置文件
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        //参数一：配置文件中的唯一标识 参数二：要接收的数据类型
        User user = ac.getBean("user5",User.class);
        System.out.println(user);

        User user6 = ac.getBean("user6",User.class);
        System.out.println(user6);
    }
}
