package cn.yunhe.test;

import cn.yunhe.dao.IUserDao;
import cn.yunhe.dao.impl.UserDaoImpl;
import cn.yunhe.service.UserService;
import cn.yunhe.service.UserService2;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class UserTest {

    /**
     * 添加用户信息
     *  1.需要执行数据层中的insertUser方法
     *  2.具体对数据层中的方法的调用通过业务层进行调用
     *  3.在控制层（此处为测试类）调用业务层中的方法
     */
    @Test
    public void insertTest(){
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext4.xml");

        //需要有userService对象
        UserService2 userService = ac.getBean("userService", UserService2.class);
        //userService.insertUser();

        //关闭读取对象
        ac.close();
    }
}
