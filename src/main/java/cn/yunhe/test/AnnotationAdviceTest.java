package cn.yunhe.test;

import cn.yunhe.dao.impl.ItemDaoImpl;
import cn.yunhe.dao.impl.ItemDaoImpl2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Juni
 * @date 2021/1/20
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext9.xml")
public class AnnotationAdviceTest {

    @Autowired
    ItemDaoImpl itemDao;

    @Test
    public void adviceTest(){
        itemDao.deleteItem(1);
    }
}
