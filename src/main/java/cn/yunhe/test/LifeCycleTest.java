package cn.yunhe.test;

import cn.yunhe.beans.Item;
import cn.yunhe.service.ItemService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class LifeCycleTest {

    @Test
    public void lifeTest(){
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext6.xml");
        Item item = ac.getBean("item", Item.class);
        Item item2 = ac.getBean("item", Item.class);
        System.out.println(item == item2);
        ac.close();
    }

    @Test
    public void lifeTest2(){
        ClassPathXmlApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext6.xml");
        ItemService itemService = ac.getBean("itemService",ItemService.class);
        ItemService itemService2 = ac.getBean("itemService",ItemService.class);
        System.out.println(itemService == itemService2);
        ac.close();
    }
}
