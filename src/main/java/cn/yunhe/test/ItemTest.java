package cn.yunhe.test;

import cn.yunhe.service.ItemService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Juni
 * @date 2021/1/19
 * Spring-test整合了JUnit测试，在进行测试时，需要指定测试流程按照Spring指定的测试流程（先读配置文件-框架自己去读）去执行
 */
//指定按照Spring给定的测试类进行测试
@RunWith(SpringJUnit4ClassRunner.class)
//指定配置文件所在的路径（classpath默认从项目根路径下查找）
@ContextConfiguration(locations = "classpath:applicationContext5.xml")
public class ItemTest {

    @Autowired
    ItemService itemService;

    @Test
    public void deleteTest(){
        itemService.deleteItem(1);
    }
}
