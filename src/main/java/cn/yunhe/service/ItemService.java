package cn.yunhe.service;

import cn.yunhe.dao.ItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Juni
 * @date 2021/1/19
 */
@Service("itemService")
@Scope(value = "prototype")
public class ItemService {

    @Autowired
    @Qualifier(value = "itemDao")
    //@Resource(name = "itemDao")
    ItemDao itemDao;

    public int deleteItem(int itemId){
        return itemDao.deleteItem(itemId);
    }
}
