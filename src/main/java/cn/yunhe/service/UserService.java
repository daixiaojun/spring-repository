package cn.yunhe.service;

import cn.yunhe.beans.User;
import cn.yunhe.dao.IUserDao;

/**
 * @author Juni
 * @date 2021/1/19
 */
public class UserService {

    //要把对象的创建交给Spring处理
    private IUserDao userDao;

    private User user2;

    public UserService(){
        System.out.println("-----UserService 无参-----");
    }

    public UserService(IUserDao userDao,User user){
        System.out.println("-----UserService 有参-----");
        this.userDao = userDao;
        this.user2 = user;
    }

    public void insertUser(){
        userDao.insertUser(user2);
    }

    public IUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }

    public User getUser() {
        return user2;
    }

    public void setUser(User user) {
        this.user2 = user;
    }
}
