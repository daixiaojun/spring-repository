package cn.yunhe.service;

import cn.yunhe.beans.User;
import cn.yunhe.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.annotation.Resources;


/**
 * @author Juni
 * @date 2021/1/19
 */
@Component("userService")
public class UserService2 {
    //根据类型注入
    //@Autowired
    //根据名字注入
    @Resource(name = "userDao2")
    private IUserDao userDao;

    public void deleteUser(){
        userDao.deleteUser(1);
    }

}
