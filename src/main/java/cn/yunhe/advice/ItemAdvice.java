package cn.yunhe.advice;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author Juni
 * @date 2021/1/20
 */
public class ItemAdvice {

    /**
     * 执行目标对象功能之前要做的事情
     */
    public void before(){
        System.out.println("去哪能买到，还不用出门，买到的真不真");
    }

    /**
     * 执行目标对象功能之后要做的事情
     */
    public void after(){
        System.out.println("验证真假，消毒等等");
    }

    /**
     * 出现异常后要做的事情
     */
    public void exception(){
        System.out.println("功能异常日志");
    }

    /**
     * 返回结果后要做的事情
     */
    public void returnMethod(){
        System.out.println("方法返回结果之后要做的事情");
    }

    /**
     * 环绕通知
     * @param proceedingJoinPoint 包含了目标对象的相关信息
     *  该通知的返回值类型要和目标方法的返回值类型保持一致
     */
    public Object around(ProceedingJoinPoint proceedingJoinPoint){
        try {
            System.out.println("环绕通知：目标方法执行之前要做的事情");
            proceedingJoinPoint.proceed();
            System.out.println("环绕通知：目标方法执行之后要做的事情");
        } catch (Throwable throwable) {
            System.out.println("环绕通知：出现异常后要做的事情");
            throwable.printStackTrace();
        } finally {
            System.out.println("环绕通知：目标方法返回结构之后要做的事情");
        }
        return 0;
    }
}
